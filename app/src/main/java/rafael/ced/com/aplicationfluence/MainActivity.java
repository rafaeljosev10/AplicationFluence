package rafael.ced.com.aplicationfluence;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private EditText edtTextoInterpretado;
    private ProgressBar audioProgressBar;
    private TextView tvEstado;
    private TextView tvTimer;
    private FloatingActionButton facGravar;
    private Boolean gravando = false ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtTextoInterpretado = (EditText) findViewById(R.id.edtTexto);
        tvEstado  = (TextView) findViewById(R.id.tvEstado);
        tvTimer = (TextView) findViewById(R.id.tvTimer);
        audioProgressBar = (ProgressBar) findViewById(R.id.audioProgressBar);
        facGravar = (FloatingActionButton) findViewById(R.id.facGravar);

        /** Configuração inicial **/
        audioProgressBar.setProgress(1);
        edtTextoInterpretado.setEnabled(false);

        /** Início de tuto, o botão ao ser clicado
         *  deve trocar a imagem de fundo e assumir
         *  a função de interrupção da gravação,
         *  alem de iniciar o timer e a gravação do audio
         **/
        facGravar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!gravando) {
                    gravando = !gravando;
                    tvEstado.setText(R.string.estado_gravando);
                    facGravar.setImageResource(R.drawable.stop);

                } else {
                    gravando = !gravando;
                    tvEstado.setText("");
                    facGravar.setImageResource(R.drawable.microphone);
                }

            }
        });


    }
}


